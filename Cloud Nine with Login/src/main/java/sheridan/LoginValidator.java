package sheridan;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class LoginValidator {
	
	private static final String REGEX = "[a-zA-Z]{1}[a-zA-Z0-9]{5,}";

	public static boolean isValidLoginName( String loginName ) {
		
		Pattern pattern = Pattern.compile(REGEX);
		Matcher match = pattern.matcher(loginName);
		
		if(!match.matches()) {
			throw new IllegalArgumentException("Login contains special characters");
		}
		
		if(loginName.length() < 6) {
			throw new IllegalArgumentException("Login not long enough");
		}
		
		return true;
	}
	
}
