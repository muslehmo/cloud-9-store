package sheridan;

import static org.junit.Assert.*;

import org.junit.Test;

public class LoginValidatorTest {

	@Test
	public void testIsValidLoginLengthRegular( ) {
		assertTrue("Invalid login" , LoginValidator.isValidLoginName( "Mohammed" ) );
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void testIsValidLoginLengthException() {
		assertTrue("Invalid login", LoginValidator.isValidLoginName( "a" ));
	}
	
	@Test
	public void testIsValidLoginLengthBoundaryIn() {
		assertTrue("Invalid login" , LoginValidator.isValidLoginName( "ramses" ) );
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void testIsValidLoginLengthBoundaryOut() {
		assertTrue("Invalid login", LoginValidator.isValidLoginName( "rames" ));
	}
	
	@Test
	public void testIsValidLoginCharactersRegular() {
		assertTrue("Invalid login" , LoginValidator.isValidLoginName( "Mohammed9" ) );
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void testIsValidLoginCharactersException() {
		assertTrue("Invalid login", LoginValidator.isValidLoginName( "!!!!!" ));
	}
	
	@Test
	public void testIsValidLoginCharactersBoundaryIn() {
		assertTrue("Invalid login" , LoginValidator.isValidLoginName( "r4amse4" ) );
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void testIsValidLoginCharactersBoundaryOut() {
		assertTrue("Invalid login", LoginValidator.isValidLoginName( "4amess" ));
	}

}
